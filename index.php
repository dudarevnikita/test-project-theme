<?php
/**
 * Index file
 *
 * @package WordPress
 */
get_header(); ?>
<div class="content">

  <div class="container">
    <div class="row">

      <div class="col-12 col-sm-12 col-md-12 col-lg-12" id="content">
      
      <?php while( have_posts() ): ?>

        <div class="page-title">
          <h1><?php the_title(); ?></h1>
        </div>

        <div class="text-wrapper">
           <?php
            the_post();
            the_content();
           ?>
        </div>
        
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>

      </div>

    </div>
  </div>

</div>
<?php get_footer(); ?>