<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <title><?php wp_title() ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php do_action( 'before_main_content_page' ); ?>

    <div class="wrap-menu header">
        <?php wp_nav_menu( array('menu' => 'Primary' )); ?>
    </div>