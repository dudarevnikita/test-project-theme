<?php
/**
 * Template Name: Front Page
 *
 * @package WordPress
 * @subpackage Test Task
 */

do_action( 'before_main_content' );

$query_last = apply_filters( 'query_last_posts', true );
$query_populars = apply_filters( 'query_popular_posts', true );

get_header(); ?>

<div class="container">
    <div id="latest" class="row">
        <div class="col-12">
            <h1><?php _e( 'Latest posts', 'test-theme' ) ?></h1>
        </div>
        <?php if ( $query_last->have_posts() ): ?>
            <?php while ( $query_last->have_posts() ) : $query_last->the_post(); ?>
                <div class="col-3 post-wrap">
                    <div class="wrap-content">
                        <div class="image">
                            <?php the_post_thumbnail( 'test-thumbnails' ); ?>
                        </div>
                        <h3 class="title">
                            <?php the_title(); ?>
                        </h3>
                        <div class="content">
                            <?php the_content(); ?> 
                        </div>
                        <div class="date">
                            <?php the_time( 'F, d Y' ) ?><br>
                        </div>
                        <div class="author-name">
                            <?php the_author() ?>
                        </div>
                        <div class="author-img">
                            <?php echo get_avatar(get_the_author_meta('ID')) ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php else: ?>
            <div class="col-12">
                <h3 class="not-found"><?php _e( 'Not found!', 'test-theme' ) ?></h3>
            </div>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </div>
        
    <div id="populars" class="row">
        <div class="col-12">
            <h1><?php _e( 'Most populars', 'test-theme' ) ?></h1>
        </div>
        <?php if ( $query_populars->have_posts() ): ?>
            <?php while ( $query_populars->have_posts() ) : $query_populars->the_post(); ?>
                <div class="col-3 post-wrap">
                    <div class="wrap-content">
                        <div class="image">
                            <?php the_post_thumbnail( 'test-thumbnails' ); ?>
                        </div>
                        <h3 class="title">
                            <?php the_title(); ?>
                        </h3>
                        <div class="content">
                            <?php the_content(); ?> 
                        </div>
                        <div class="date">
                            <?php the_time( 'F, d Y' ) ?><br>
                        </div>
                        <div class="author-name">
                            <?php the_author() ?>
                        </div>
                        <div class="author-img">
                            <?php echo get_avatar(get_the_author_meta('ID')) ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php else: ?>
            <div class="col-12">
                <h3 class="not-found"><?php _e( 'Not found!', 'test-theme' ) ?></h3>
            </div>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </div>

    <div id="subscription" class="row">
        <div class="col-12">
            <div class="form-group row">
                <div class="col-12">
                    <h1><?php _e( 'Subscription form', 'test-theme' ) ?></h1>
                </div>
                <div class="col-4">
                    <input type="mail" class="form-control" placeholder="<?php _e( 'Subscribe to newsletter', 'test-theme' ) ?> ">
                </div>
                <div class="col-2">
                    <button type="submit" class="btn btn-primary"><?php _e( 'Subscribe', 'test-theme' ) ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>