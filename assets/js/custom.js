;jQuery(document).ready(function(){
    anchor_links();
    buttonToTop();

    email_subscribe();
});

/*
 * Anchor links
 */
function anchor_links(){
    jQuery('.wrap-menu .menu a[href*="#"]').click(function(event){
        jQuery('html, body').animate({
            scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
        }, 500);
        event.preventDefault();
    });
}

/**
 * Button to top script
 */
function buttonToTop() {
  jQuery(window).scroll(function(){
    if (jQuery(this).scrollTop() > 299 ) {
      jQuery('#button-top').fadeIn();
    } else {
      jQuery('#button-top').fadeOut();
    }
  });


  jQuery('#button-top').click(function(){
    jQuery('html, body').animate({scrollTop : 0},800);
    return false;
  });
}

function email_subscribe(){
    jQuery('#subscription').on('click', 'button.btn', function(e){
        var target = e.target;
        if ( jQuery('#subscription input[type=mail]').hasClass('is-invalid') ) {
            jQuery('#subscription input[type=mail]').removeClass('is-invalid');
        }
        jQuery(this).text('Wait...');
        jQuery(this).prop('disabled', true);
        jQuery.ajax({
            url: ajax_var.ajax_url,
            method: 'POST',
            data: {
                action: 'subscription_event',
                email: jQuery('#subscription input[type=mail]').val()
            }
        }).done(function(response){
            if ( response.success) {
                alert( response.data.message );
                jQuery('#subscription input[type=mail]').val('');
                jQuery('#subscription input[type=mail]').addClass('is-valid');
                setTimeout(function() {
                    jQuery('#subscription input[type=mail]').removeClass('is-valid');
                }, 2000)
            } else {
                alert( response.data.message );
                jQuery('#subscription input[type=mail]').addClass('is-invalid');
            }
        }).fail(function(){
            console.log("Error while request!");
        }).always(function(){
            jQuery(target).text('Subscribe');
            jQuery(target).prop('disabled', false);
        });

    });
}