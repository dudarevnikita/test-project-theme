<?php
/**
 * Enqueue theme scripts and styles
 */
function enqueue_theme_scripts() {

    /*
    * Connect Bootstrap CSS & JS
    */
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '4.0.0' );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'tether' ), '4.0.0', true );
    wp_enqueue_script( 'tether', get_template_directory_uri() . '/assets/js/tether.min.js', array( 'jquery' ), '1.4.3', true );

    /**
    * Font awesome
    */
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.7.0' );

    /*
    * Theme styles
    */
    wp_enqueue_style( 'custom_theme', get_template_directory_uri() . '/assets/css/custom.css', array(), '1.0.0' );
    /*
    * Theme scripts
    */
    wp_enqueue_script( 'custom_theme_js', get_template_directory_uri() . '/assets/js/custom.js', array(), '1.0.0', true );

    wp_localize_script( 'custom_theme_js', 'ajax_var', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_scripts' );

/**
 * Menu support
 */
function theme_register_nav_menu()
{
    register_nav_menu( 'primary', 'Primary Menu' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu' );

/**
 * Theme translate
 */
function load_theme_textdomain_function()
{
  load_theme_textdomain( 'test-theme', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'load_theme_textdomain_function' );


/**
 * Theme support
 */
function post_thumb()
{
    if ( function_exists( 'add_image_size' ) ) {
        add_image_size( 'test-thumbnails', 250, 100, true );
        add_theme_support('post-thumbnails');
    }
}
add_action( 'init', 'post_thumb' );

/**
 * WP Query for latest posts
 */
function query_last_posts( $args )
{
    $args = array(
        'posts_per_page' => 16,
        'orderby' => 'date',
        'order' => 'ASC',
        'post_type' => 'post',
    );
    return new WP_Query( $args );
}
add_filter( 'query_last_posts', 'query_last_posts', 10 );

/**
 * WP Query for most popular posts
 */
function query_popular_posts( $args )
{
    $args = array(
        'posts_per_page' => 8,
        'meta_key' => '_post_views_count',
        'orderby' => 'meta_value_num',
        'post_type' => 'post',
    );
    return new WP_Query( $args );
}
add_filter( 'query_popular_posts', 'query_popular_posts', 10 );

/**
 * track popularity of the post
 */
function track_post_views ( $post_id )
{
    if ( ! is_single() ) return;
    if ( empty ( $post_id ) ) {
        global $post;
        $post_id = $post->ID;    
    }
    set_post_views($post_id);
}
add_action( 'wp_head', 'track_post_views');

function set_post_views( $postID )
{
    $count_key = '_post_views_count';
    $count = get_post_meta( $postID, $count_key, true );
    if( empty( $count ) ) {
        $count = 0;
        delete_post_meta( $postID, $count_key );
        add_post_meta( $postID, $count_key, '1' );
    } else {
        $count++;
        update_post_meta( $postID, $count_key, $count );
    }
}

/**
 * Subscription ajax event hendler
 */
function subscription_event()
{
    $email = $_POST['email'];
    if ( ! empty( $email ) ) {
        if ( preg_match( '/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1}))@([-0-9A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/', $email ) ) {
            if ( send_mail_function( $email ) ) {
                $data = array(
                    'message' => __( 'Thanks for the subscription!', 'test-theme' )
                );
                wp_send_json_success( $data );
            } else {
                $data = array(
                    'message' => __( 'Something is wrong', 'test-theme' )
                );
                wp_send_json_error( $data );
            }
        } else {
            $data = array(
                'message' => __( 'Invalid email', 'test-theme' )
            );
            wp_send_json_error( $data );
        }
    } else {
        $data = array(
            'message' => __( 'Enter e-mail', 'test-theme' )
        );
        wp_send_json_error( $data );
    }
}
add_action( 'wp_ajax_subscription_event', 'subscription_event' );
add_action( 'wp_ajax_nopriv_subscription_event', 'subscription_event' );

function send_mail_function( $email )
{
    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
    ob_start();
    $headers = 'From: '. get_bloginfo('name') .' <contact-form@'. get_site_url() .'>' . "\r\n";
    $headers = ob_get_clean();

    $content = __( 'Thanks for the subscription!', 'test-theme' );

    $attachment = array( get_template_directory() . '/assets/pdf/test.pdf' );

    $time = current_time('d/m/Y H:i');

    if ( wp_mail( $email, __( 'Subscription ', 'test-theme' ) . $time, $content, $headers, $attachment ) ) {
        save_data_email( $email, $time, 'success' );
        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
        return true;
    } else {
        save_data_email( $email, $time, 'error' );
        remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
        return false;
    }

}

/**
 * Set html content type
 */
function set_html_content_type()
{
  return 'text/html';
}

function save_data_email( $email, $time, $message )
{
    $mail_array = get_option( 'send_email', true );
    if( $mail_array === 1 ) {
        $args = array(
            array(
                'email'     => $email,
                'time'      => $time,
                'status'    => $message
            )
        );
        delete_option( 'send_email' );
        update_option( 'send_email', $args );
    } else {
        $mail_array[] = array(
            'email'     => $email,
            'time'      => $time,
            'status'    => $message
        );
        update_option( 'send_email', $mail_array );
    }
}

/**
 * Adding a page to the admin panel
 */
function add_menu_page_function()
{
    add_menu_page(
        __( 'Subscribers', 'test-theme' ),
        __( 'Subscribers', 'test-theme' ),
        'manage_options',
        'form-subscribers',
        'display_email',
        'dashicons-email-alt',
        30 
    );
}
add_action( 'admin_menu', 'add_menu_page_function' );

/**
 * Display function
 */
function display_email()
{
    $email_array = get_option( 'send_email', true );
    ?>
    <div class="wrap">
        <h2><?php echo get_admin_page_title() ?></h2>

        <?php if ( $email_array !== 1 ): ?>
            <table>
                <thead>
                    <tr>
                        <td><?php _e( '#', 'test-theme' ) ?></td>
                        <td><?php _e( 'Email', 'test-theme' ) ?></td>
                        <td><?php _e( 'Date & Time', 'test-theme' ) ?></td>
                        <td><?php _e( 'Status', 'test-theme' ) ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($email_array as $key => $value): ?>
                        <tr>
                            <td class="column-columnname"><?php echo ++$key ?></td>
                            <td class="column-columnname"><?php echo $value['email'] ?></td>
                            <td class="column-columnname"><?php echo $value['time'] ?></td>
                            <td class="column-columnname"><?php echo $value['status'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <?php _e( 'No Subscribers', 'test-theme' ) ?>
        <?php endif; ?>
    </div>
    <?php
}

/**
 * Initializing styles for admin panel
 */
function add_admin_scripts()
{
    if( $_GET['page'] == 'form-subscribers' ) {
        wp_enqueue_style( 'form-subscribers', get_template_directory_uri() . '/assets/css/admin.css' );
    }
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts' );