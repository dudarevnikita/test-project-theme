#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Test Task Theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-25 19:03+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#. URI of the theme
#. Author URI of the theme
#: functions.php:246
msgid "#"
msgstr ""

#: functions.php:248
msgid "Date & Time"
msgstr ""

#: functions.php:247
msgid "Email"
msgstr ""

#: functions.php:151
msgid "Enter e-mail"
msgstr ""

#. Name of the template
msgid "Front Page"
msgstr ""

#: 404.php:17
msgid "Home"
msgstr ""

#: functions.php:145
msgid "Invalid email"
msgstr ""

#: page-template-main.php:19
msgid "Latest posts"
msgstr ""

#: page-template-main.php:56
msgid "Most populars"
msgstr ""

#. Author of the theme
msgid "Nikita Dudarev"
msgstr ""

#: functions.php:264
msgid "No Subscribers"
msgstr ""

#: page-template-main.php:48 page-template-main.php:85
msgid "Not found!"
msgstr ""

#: 404.php:15
msgid "Oops!!!"
msgstr ""

#: 404.php:16
msgid "Page not found"
msgstr ""

#: functions.php:139
msgid "Something is wrong"
msgstr ""

#: functions.php:249
msgid "Status"
msgstr ""

#: page-template-main.php:101
msgid "Subscribe"
msgstr ""

#: page-template-main.php:98
msgid "Subscribe to newsletter"
msgstr ""

#: functions.php:221 functions.php:222
msgid "Subscribers"
msgstr ""

#: functions.php:172
msgid "Subscription "
msgstr ""

#: page-template-main.php:95
msgid "Subscription form"
msgstr ""

#. Name of the theme
msgid "Test Task Theme"
msgstr ""

#: functions.php:134 functions.php:166
msgid "Thanks for the subscription!"
msgstr ""
