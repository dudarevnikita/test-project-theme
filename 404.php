<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 */

get_header(); ?>

<div class="content 404">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center error-page">
        <h1 class="title-error">404</h1>
        <h2 class="text-error"><?php _e( 'Oops!!!', 'test-theme' ) ?></h2>
        <p class="description-error"><?php _e( 'Page not found', 'test-theme' ) ?></p>
        <a href="<?php echo home_url( '/' ) ?>" class="button animate-hover"><?php _e( 'Home', 'test-theme' ) ?></a>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
